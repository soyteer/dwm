/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 2;        /* gaps between windows */
static const unsigned int snap      = 0;        /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;        /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Open Sans SemiBold:size=9.5", "JoyPixels:size=8:antialias=true:autohint=true", "Hack Nerd Font Mono:size=14:antialias=true:autohint=true" };
static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee";
static char selbordercolor[]        = "#005577";
static char selbgcolor[]            = "#005577";
static char *colors[][3] = {
  /*               fg           bg           border   */
  [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
  [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};

typedef struct {
  const char *name;
  const void *cmd;
} Sp;
const char *spcmd1[] = {"st", "-n", "spterm", "-g", "120x34", NULL };
const char *spcmd2[] = {"st", "-n", "spfm", "-g", "144x41", "-e", "lf", NULL };
/* const char *spcmd2[] = {"pcmanfm", NULL }; */
const char *spcmd3[] = {"keepassxc", NULL };
static Sp scratchpads[] = {
  /* name          cmd  */
  {"spterm",      spcmd1},
  {"spranger",    spcmd2},
  {"keepass",     spcmd3},
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

/* first element is for all-tag view */
/* static int defaultlayouts[1 + LENGTH(tags)]  = { 0, 0, 0, 0, 0, 3, 0, 0, 0, 0}; */

static const MonitorRule monrules[] = {
  /* monitor  tag  layout  mfact  nmaster  showbar  topbar */
  /* {  1,       -1,  2,      -1,    -1,      -1,      -1     }, // use a different layout for the second monitor */
  {  -1,       5,    3,      0.50,  -1,      -1,      -1     }, // set the media board on tag five
  {  -1,      -1,    0,      -1,    -1,      -1,      -1     }, // default
};

static const Rule rules[] = {
  /* xprop(1):
   *  WM_CLASS(STRING) = instance, class
   *  WM_NAME(STRING) = title
   */
  /* 1;class 2;instance 3;title 4;tagsmask 5;switch 6;isfloating 7;isterminal 8;noswallow 9;monitor*/
  /* 1                2           3               4       5  6  7  8    9*/
  { "Gimp",           NULL,       NULL,           0,      0, 0, 0, 1,  -1 },
  { "Float",          NULL,       NULL,           0,      0, 1, 0, 1,  -1 }, /* Generic floating solution */
  { "MEGAsync",       NULL,       NULL,           0,      0, 1, 0, 1,  -1 },
  { "Steam",          NULL,       NULL,           1 << 8, 0, 0, 0, 1,  -1 },
  { NULL,             NULL,       "Steam",        1 << 8, 0, 0, 0, -1, -1 },
  { "Brave-browser",  NULL,       NULL,           1 << 0, 1, 0, 0, -1, -1 },
  { "Chromium",       NULL,       NULL,           1 << 0, 1, 0, 0, -1, -1 },
  { "qutebrowser",    NULL,       NULL,           1 << 0, 1, 0, 0, -1, -1 },
  { "LibreWolf",      NULL,       NULL,           1 << 0, 1, 0, 0, -1, -1 },
  { "Thunderbird",    NULL,       NULL,           1 << 5, 1, 0, 0, 0,  -1 },
  { "Nemo",           NULL,       NULL,           1 << 1, 1, 0, 0, 0,  -1 },
  { "pcmanfm",        NULL,       NULL,           1 << 1, 1, 0, 0, 0,  -1 },
  { "lf",             NULL,       NULL,           1 << 1, 1, 0, 1, 0,  -1 },
  { "nvim",           NULL,       NULL,           1 << 2, 1, 0, 0, 0,  -1 },
  { "File-roller",    NULL,       NULL,           1 << 1, 0, 0, 0, 0,  -1 },
  { "discord",        NULL,       NULL,           1 << 5, 0, 0, 0, 0,  -1 },
  { "Ripcord",        NULL,       NULL,           1 << 5, 0, 0, 0, 0,  -1 },
  { "cmus",           NULL,       NULL,           1 << 4, 0, 0, 1, 0,  -1 },
  { "ncmpcpp",        NULL,       NULL,           1 << 4, 0, 0, 1, 0,  -1 },
  { "tuir",           NULL,       NULL,           1 << 4, 0, 0, 1, 0,  -1 },
  { "newsboat",       NULL,       NULL,           1 << 4, 0, 0, 1, 0,  -1 },
  { "St",             NULL,       NULL,           0,      0, 0, 1, 0,  -1 },
  { NULL,             NULL,       "Event Tester", 0,      0, 0, 0, 1,  -1 },
  { NULL,             "spterm",   NULL,         SPTAG(0), 0, 1, 0, 0,  -1 },
  { NULL,             "spfm",     NULL,         SPTAG(1), 0, 1, 0, 0,	 -1 },
  { NULL,             "keepass",  NULL,         SPTAG(2), 0, 0, 0, 0,	 -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
  /* symbol   arrange function */
  { "﬿",      tile },    /* first entry is default */
  { "",      monocle },
  { "",      NULL },    /* no layout function means floating behavior */
  { "𤋮",     bstack }, /* bottom stack */
  { "===",    bstackhoriz }, /* bottom stack horizontaly */
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

#include <X11/XF86keysym.h>

static Key keys[] = {
  /* modifier             key             function        argument */
  { MODKEY|ShiftMask,     XK_b,           togglebar,      {0} },
  { MODKEY|ShiftMask,     XK_j,           rotatestack,    {.i = +1 } },
  { MODKEY|ShiftMask,     XK_k,           rotatestack,    {.i = -1 } },
  { MODKEY,               XK_j,           focusstack,     {.i = +1 } },
  { MODKEY,               XK_k,           focusstack,     {.i = -1 } },
  { MODKEY,               XK_i,           incnmaster,     {.i = +1 } },
  { MODKEY,               XK_d,           incnmaster,     {.i = -1 } },
  { MODKEY|ShiftMask,     XK_h,           setmfact,       {.f = -0.05} },
  { MODKEY|ShiftMask,     XK_l,           setmfact,       {.f = +0.05} },
  { MODKEY,               XK_space,       zoom,           {0} },
  { MODKEY,               XK_Tab,         view,           {0} },
  { MODKEY,               XK_apostrophe,  killclient,     {0} },
  { MODKEY,               XK_KP_End,      setlayout,      {.v = &layouts[0]} },
  { MODKEY,               XK_KP_Down,     setlayout,      {.v = &layouts[1]} },
  { MODKEY,               XK_KP_Next,     setlayout,      {.v = &layouts[2]} },
  { MODKEY,               XK_KP_Left,     setlayout,      {.v = &layouts[3]} },
  /* { MODKEY,               XK_KP_Begin,    setlayout,      {.v = &layouts[4]} }, */
  { MODKEY,               XK_x,           setlayout,      {0} },
  { MODKEY|ShiftMask,     XK_space,       togglefloating, {0} },
  { MODKEY,               XK_0,           view,           {.ui = ~0 } },
  { MODKEY|ShiftMask,     XK_0,           tag,            {.ui = ~0 } },
  { MODKEY,               XK_comma,       focusmon,       {.i = -1 } },
  { MODKEY,               XK_period,      focusmon,       {.i = +1 } },
  { MODKEY|ShiftMask,     XK_comma,       tagmon,         {.i = -1 } },
  { MODKEY|ShiftMask,     XK_period,      tagmon,         {.i = +1 } },
  { MODKEY|Mod1Mask,      XK_s,           spawn,          SHCMD("sbrd") },
  { MODKEY|Mod1Mask,      XK_d,           spawn,          SHCMD("dlmgr -ax && pkill -RTMIN+9 dwmblocks") },
  { MODKEY|Mod1Mask|ShiftMask, XK_d,      spawn,          SHCMD("dlmgr -mx && pkill -RTMIN+9 dwmblocks") },

  { MODKEY,               XK_Return,      spawn,          SHCMD("open-term") },
  { MODKEY|ShiftMask,     XK_Return,      spawn,          SHCMD("/usr/bin/st -e /usr/bin/su") },
  { MODKEY,               XK_r,           spawn,          SHCMD("dmenu_run_history -l 3 -g 7 -h 20 -p run") },
  { MODKEY|ControlMask,   XK_space,       spawn,          SHCMD("dmenu_desktop") },
  { MODKEY|ShiftMask,     XK_q,           spawn,          SHCMD("xlogout") },

  /* { MODKEY,               XK_m,           spawn,          SHCMD("open-term cmus") }, */
  { MODKEY,               XK_m,           spawn,          SHCMD("open-term ncmpcpp") },
  { MODKEY,               XK_n,           spawn,          SHCMD("open-term newsboat") },
  { MODKEY,               XK_t,           spawn,          SHCMD("open-term htop") },
  { MODKEY|ShiftMask,     XK_r,           spawn,          SHCMD("open-term tuir") },
  { MODKEY,               XK_f,           spawn,          SHCMD("$FILE") },
  { MODKEY|ShiftMask,     XK_f,           spawn,          SHCMD("pcmanfm") },
  { MODKEY,               XK_b,           spawn,          SHCMD("$BROWSER") },
  { MODKEY,               XK_c,           spawn,          SHCMD("dmenu_file_history") },
  { MODKEY|ShiftMask,     XK_y,           spawn,          SHCMD("open-term ytfzf") },
  { MODKEY|ShiftMask,     XK_a,           spawn,          SHCMD("dmenu_anime") },
  { MODKEY|ShiftMask,     XK_d,           spawn,          SHCMD("dmenu_doom") },
  { MODKEY|ShiftMask,     XK_g,           spawn,          SHCMD("dmenu_lutris") },
  { MODKEY|ShiftMask,     XK_u,           spawn,          SHCMD("dmenu_unicode") },
  { MODKEY|ShiftMask,     XK_v,           spawn,          SHCMD("vim-diesel") },
  { MODKEY|ShiftMask,     XK_w,           spawn,          SHCMD("bgpick") },
  { MODKEY|ShiftMask,     XK_p,           spawn,          SHCMD("passmenu2") },

  { MODKEY,             XK_bracketright,  spawn,          SHCMD("pamixer --allow-boost -i 5; kill -44 $(pidof dwmblocks)") },
  { MODKEY,             XK_bracketleft,   spawn,          SHCMD("pamixer --allow-boost -d 5; kill -44 $(pidof dwmblocks)") },
  { MODKEY|ShiftMask,   XK_bracketright,  spawn,          SHCMD("mpc volume +10") },
  { MODKEY|ShiftMask,   XK_bracketleft,   spawn,          SHCMD("mpc volume -10") },
  { MODKEY,             XK_period,        spawn,          SHCMD("mpc next; kill -45 $(pidof dwmblocks)") },
  { MODKEY,             XK_comma,         spawn,          SHCMD("mpc prev; kill -45 $(pidof dwmblocks)") },

  { 0,          XF86XK_AudioMute,         spawn,          SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)") },
  { 0,          XF86XK_AudioRaiseVolume,  spawn,          SHCMD("pamixer --allow-boost -i 5; kill -44 $(pidof dwmblocks)") },
  { 0,          XF86XK_AudioLowerVolume,  spawn,          SHCMD("pamixer --allow-boost -d 5; kill -44 $(pidof dwmblocks)") },
  { 0,          XF86XK_AudioPrev,         spawn,          SHCMD("mpc prev; kill -45 $(pidof dwmblocks)") },
  { 0,          XF86XK_AudioNext,         spawn,          SHCMD("mpc next; kill -45 $(pidof dwmblocks)") },
  { 0,          XF86XK_AudioPlay,         spawn,          SHCMD("mpc toggle; kill -45 $(pidof dwmblocks)") },
  { 0,          XF86XK_AudioStop,         spawn,          SHCMD("mpc stop; kill -45 $(pidof dwmblocks)") },
  { MODKEY,     XF86XK_AudioRaiseVolume,  spawn,          SHCMD("mpc volume +10") },
  { MODKEY,     XF86XK_AudioLowerVolume,  spawn,          SHCMD("mpc volume -10") },

  { 0,                    XK_Print,       spawn,          SHCMD("takepic -n") },
  { ShiftMask,            XK_Print,       spawn,          SHCMD("takepic -d") },
  { MODKEY,               XK_Print,       spawn,          SHCMD("takepic -s") },

  { ControlMask|Mod1Mask, XK_BackSpace,   spawn,          SHCMD("killall -9 Xorg") }, /* Kill switch */

  { MODKEY,               XK_F3,          spawn,          SHCMD("pgrep -x nmtui-connect && pkill nmtui-connect || st -g 120x34 -c Float -e nmtui-rescan") },
  { MODKEY,               XK_F4,          spawn,          SHCMD("pgrep -x pulsemixer && pkill pulsemixer || st -g 120x34 -c Float -e pulsemixer") },
  { MODKEY,               XK_F5,          xrdb,           {.v = NULL } },
  { MODKEY,               XK_F6,          spawn,          SHCMD("mpc toggle; kill -45 $(pidof dwmblocks)") },
  { MODKEY,               XK_F7,          spawn,          SHCMD("mpc stop; kill -45 $(pidof dwmblocks)") },
  { MODKEY,               XK_F8,          spawn,          SHCMD("pulsemixer --toggle-mute && pkill -RTMIN+10 dwmblocks") },
  { MODKEY,               XK_F9,          spawn,          SHCMD("dmenu_mount") },
  { MODKEY,               XK_F10,         spawn,          SHCMD("dmenu_umount") },

  { MODKEY,               XK_s,           togglescratch,  {.ui = 0 } },
  { MODKEY,               XK_u,           togglescratch,  {.ui = 1 } },
  { MODKEY,               XK_z,           togglescratch,  {.ui = 2 } },

  TAGKEYS(                XK_1,                           0)
  TAGKEYS(                XK_2,                           1)
  TAGKEYS(                XK_3,                           2)
  TAGKEYS(                XK_4,                           3)
  TAGKEYS(                XK_5,                           4)
  TAGKEYS(                XK_6,                           5)
  TAGKEYS(                XK_7,                           6)
  TAGKEYS(                XK_8,                           7)
  TAGKEYS(                XK_9,                           8)
  { MODKEY|ShiftMask,     XK_End,         quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
  /* click                event mask      button          function        argument */
  { ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
  { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
  { ClkWinTitle,          0,              Button2,        zoom,           {0} },
  { ClkStatusText,        0,              Button1,        sigdwmblocks,   {.i = 1} },
  { ClkStatusText,        0,              Button2,        sigdwmblocks,   {.i = 2} },
  { ClkStatusText,        0,              Button3,        sigdwmblocks,   {.i = 3} },
  { ClkStatusText,        0,              Button4,        sigdwmblocks,   {.i = 4} },
  { ClkStatusText,        0,              Button5,        sigdwmblocks,   {.i = 5} },
  { ClkStatusText,        ShiftMask,      Button1,        sigdwmblocks,   {.i = 6} },
  { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
  { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
  { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
  { ClkTagBar,            0,              Button1,        view,           {0} },
  { ClkTagBar,            0,              Button3,        toggleview,     {0} },
  { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
  { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
