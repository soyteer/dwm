# This is my build of dwm

Nothing special here just has some stuff to make it look a little cool like

* pertag
* rotatestack
* systray
* statuscmd
* swallow
* scratchpads
* attachaside
* switchtotag
* xrdb

# Installing

It's suckless, you should know, if not then well then run:

```
$ make PREFIX=/usr clean install
$ cd dwmblocks
$ make PREFIX=/usr clean install
```

# dwmblocks

dwm is my status bar of choice, since my dwm is pretty much 'tied' with this version of dwmblocks, the config.h is also available here.

Requires libxft-brga for color emojis.
